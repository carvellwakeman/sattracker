#include <math.h>

#include <AccelStepper.h> //to drive each stepper
#include <MultiStepper.h> //to synchronize both steppers

// The Azimuth Stepper pins
#define AZ_DIR_PIN 16 // D0
#define AZ_STEP_PIN 5 // D1

AccelStepper AZstepper(AccelStepper::DRIVER, AZ_STEP_PIN, AZ_DIR_PIN);
long oneTurn = 16384; // Number of steps per one rotation for stepper motor.


void setup()
{
    Serial.begin(115200);

    AZstepper.setMaxSpeed(2000);
    AZstepper.setCurrentPosition(0); // Azimuth stepper starts at 0 (north)
    AZstepper.setAcceleration(40.0);
    //AZstepper.setPinsInverted(true,false,false ); //DIR pin inverted on AZ motor --> uncomment to inverse AZ motor
}

long tPos = 0; // target
long cPos = 0; // current
void loop()
{
    if (Serial.available() > 0)
    {
        tPos = Serial.parseInt();
        if (tPos != 0) {
            AZstepper.moveTo(tPos);
            Serial.println("Target Position " + String(tPos));
        }
    }

    // Adjust target position to be within range [0, oneTurn]
    while (tPos > oneTurn || tPos < 0) {
        if (tPos > oneTurn) {
            tPos -= oneTurn;
        }
        else if (tPos < 0) {
            tPos += oneTurn;
        }
        Serial.println("Adjust target position to: " + String(tPos));
        AZstepper.moveTo(tPos);
    }

    if (AZstepper.distanceToGo() != 0) {
        cPos = AZstepper.currentPosition();
        Serial.println("At " + String(cPos));

        AZstepper.setSpeed(1000);
        AZstepper.runSpeedToPosition();
    }
}