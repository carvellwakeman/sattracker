// MultiStepper.pde
// -*- mode: C++ -*-
//
// Shows how to multiple simultaneous steppers
// Runs one stepper forwards and backwards, accelerating and decelerating
// at the limits. Runs other steppers at the same time
//
// Copyright (C) 2009 Mike McCauley
// $Id: MultiStepper.pde,v 1.1 2011/01/05 01:51:01 mikem Exp mikem $

#include <AccelStepper.h>

#define AZ_DIR_PIN 16 // D0
#define AZ_STEP_PIN 5 // D1
// The Elevation stepper pins
#define EL_DIR_PIN 0  // D3
#define EL_STEP_PIN 4 // D2

// Define some steppers and the pins the will use
AccelStepper stepper1(AccelStepper::DRIVER, AZ_STEP_PIN, AZ_DIR_PIN);
// AccelStepper stepper2(AccelStepper::DRIVER, EL_STEP_PIN, EL_DIR_PIN);


void setup()
{  
    Serial.begin(115200);

    Serial.println("starting");

    stepper1.setMaxSpeed(2000.0);
    stepper1.setAcceleration(1.0);
    // stepper1.moveTo(900);
    stepper1.setSpeed(stepper1.maxSpeed());
    
    // stepper2.setMaxSpeed(300.0);
    // stepper2.setAcceleration(100.0);
    // stepper2.moveTo(1000000);
}

void loop()
{

    // Change direction at the limits
    // if (stepper1.distanceToGo() == 0) {
	//     stepper1.moveTo(-stepper1.currentPosition());
    //     stepper1.setSpeed(stepper1.maxSpeed());
    // }
    
    stepper1.runSpeed();
    // stepper1.run();
    // stepper2.run();
}
