enum CalibState
{
    BEGIN_CALIB,
    AZ_ZERO1,
    AZ_ZERO2,
    AZ_ZERO3,
    AZ_ZERO4,
    AZ_ZERO5,
    EL_ZERO1,
    EL_ZERO2,
    EL_ZERO3,
    EL_ZERO4,
    HORIZONTAL,
    NORTH
};

CalibState calibrationState = BEGIN_CALIB;

bool Calibrate()
{
    switch (calibrationState)
    {
    // Begin calibration
    case BEGIN_CALIB:
        Serial.println("Beginning calibration");

        // digitalWrite(ENABLE_PIN, LOW);        //enable

        // If dish is already in AZ sensor's range, shuffle left a bit so we can probe the left edge first
        if (digitalRead(AZ_ZERO_PIN) == LOW)
        {
            Serial.println("Already on AZ sensor, moving left to clear.");
#ifdef HAS_AZ_STEPPER
            AZstepper.setCurrentPosition(0);
            AZstepper.move((oneTurn * 45 / 360) * -1);
#endif
        }

        calibrationState = AZ_ZERO1;
        break;

    // If the sensor is hit initially, backup a bit so we can start on its left edge
    case AZ_ZERO1:
        if (AZstepper.distanceToGo() < 0)
        {
            AZstepper.setSpeed(500);
            AZstepper.runSpeedToPosition();
        }
        else
        {
            calibrationState = AZ_ZERO2;
        }
        break;

    // Run Az motor CW until hall effect sensor reached
    case AZ_ZERO2:
        if (digitalRead(AZ_ZERO_PIN) == HIGH)
        {
#ifdef HAS_AZ_STEPPER
            AZstepper.setSpeed(100);
            AZstepper.runSpeed();
        }
        else
#endif
        {
            Serial.println("Azimuth sensor reached");

#ifdef HAS_AZ_STEPPER
            AZstepper.setCurrentPosition(0);
            AZstepper.moveTo(oneTurn * 45 / 360);
            AZstepper.setSpeed(100);
#endif
            Serial.println("Beginning azimuth second pass");
            calTime = millis();
            calibrationState = AZ_ZERO3;
        }
        break;

    // Move 45 degrees past sensor
    case AZ_ZERO3:
        if (AZstepper.distanceToGo() > 0)
        {
            AZstepper.runSpeedToPosition();
        }
        else
        {
            calibrationState = AZ_ZERO4;
        }
        break;

    // Run Az motor CW until hall effect sensor reached
    case AZ_ZERO4:
        if (digitalRead(AZ_ZERO_PIN) == HIGH)
        {
#ifdef HAS_AZ_STEPPER
            AZstepper.setSpeed(-100);
            AZstepper.runSpeed();
#endif
        }
        else
        {
            Serial.println("Azimuth sensor reached (pass 2)");

#ifdef HAS_AZ_STEPPER
            AZstepper.stop(); //magnet reached
            AZstepper.moveTo(AZstepper.currentPosition() / 2);
#endif
            calTime = millis();
            calibrationState = AZ_ZERO5;
        }
        break;

    // Motor is now at Left side of sensor, with Zero as Right side of sensor
    // Move half of current position towards sensor to find center
    case AZ_ZERO5:
#ifdef HAS_AZ_STEPPER
        if (AZstepper.distanceToGo() > 0)
        {
            AZstepper.setSpeed(100);
            AZstepper.runSpeedToPosition();
        }
        else
#endif
        {
#ifdef HAS_AZ_STEPPER
            AZstepper.setCurrentPosition(0);
#endif
            calibrationState = EL_ZERO1;
            Serial.println("Azimuth motor calibrated");
        }
        break;

    // Run Elevation motor CW until first edge of hall sensor reached
    case EL_ZERO1:
#ifdef HAS_EL_STEPPER
        if (digitalRead(EL_ZERO_PIN) == HIGH)
        {
            ELstepper.setSpeed(100);
            ELstepper.runSpeed();
        }
        else
#endif
        {
            Serial.println("Elevation sensor reached");

#ifdef HAS_EL_STEPPER
            ELstepper.setCurrentPosition(0);
            ELstepper.moveTo(oneTurn * 45 / 360);
            ELstepper.setSpeed(100);
#endif
            Serial.println("Beginning elevation second pass");
            calTime = millis();
            calibrationState = EL_ZERO2;
        }
        break;

    // Move past sensor
    case EL_ZERO2:
#ifdef HAS_EL_SENSOR
        if (ELstepper.distanceToGo() > 0)
        {
            ELstepper.runSpeedToPosition();
        }
        else
#endif
        {
            calibrationState = EL_ZERO3;
        }
        break;

    // Reach other edge of the sensor
    case EL_ZERO3:
#ifdef HAS_EL_STEPPER
        if (digitalRead(EL_ZERO_PIN) == HIGH)
        {
            ELstepper.setSpeed(-100);
            ELstepper.runSpeed();
        }
        else
#endif
        {
            Serial.println("Elevation sensor reached (pass 2)");

#ifdef HAS_EL_STEPPER
            ELstepper.stop(); //magnet reached
            ELstepper.moveTo(ELstepper.currentPosition() / 2);
#endif
            calTime = millis();
            calibrationState = EL_ZERO4;
        }
        break;

    // Move to halfway between both edges of the sensor
    case EL_ZERO4:
#ifdef HAS_EL_STEPPER:
        if (ELstepper.distanceToGo() > 0)
        {
            ELstepper.setSpeed(100);
            ELstepper.runSpeedToPosition();
        }
        else
#endif
        {
#ifdef HAS_EL_STEPPER:
            ELstepper.setCurrentPosition(oneTurn * 90 / 360);
            ELstepper.moveTo(0);
#endif
            calibrationState = HORIZONTAL;
            Serial.println("Elevation motor calibrated");
        }
        break;

    // Move antenna to point straight forward
    case HORIZONTAL:
#ifdef HAS_EL_STEPPER
        if (ELstepper.distanceToGo() > 0)
        {
            ELstepper.setSpeed(100);
            ELstepper.runSpeedToPosition();
        }
        else
#endif
        {
            Serial.println("Antenna horizontal");
            calTime = millis();
            calibrationState = NORTH;
        }
        break;

    // Find north (TODO: No compass sensor)
    case NORTH:
        Serial.println("Antenna pointing North");
        Serial.println("Calibration Complete");
        return true;
    }

    return false;
}