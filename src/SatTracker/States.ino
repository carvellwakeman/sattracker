boolean dirCW = true;
boolean posCW = true;

enum TrackState
{
    STANDBY,
    PREPASS,
    INPASS,
    POSTPASS,
    ENDPASS
};

TrackState trackState = STANDBY;

bool track()
{
    switch (trackState)
    {
    case STANDBY:
        // digitalWrite(ENABLE_PIN, HIGH); //disable

#ifdef DEBUG
        if ((millis() - lastDebug) > 30000)
        {
            lastDebug = millis();
            Serial.println("Standby. " + String(satname) + " will pass in: " + String(nextpassEpoch - timeNow));
        }
#endif

        //one minute before pass (and less than 5s after start)
        if (nextpassEpoch - timeNow < 60 && nextpassEpoch + 5 - timeNow > 0)
        {
            trackState = PREPASS;
        }

        break;

    case PREPASS:
        // Pass is less than 60 seconds away, move antenna to start location and wait.
        passStatus = 1;
        if ((AZstart < 360) && (AZstart > 180))
            AZstart = AZstart - 360; //Goes to start counter-clockwise if closer.
        if (AZstart < 0)
            posCW = false;
        else
            posCW = true;
#ifdef HAS_AZ_STEPPER
        // digitalWrite(ENABLE_PIN, LOW); //enable
        AZstepper.runToNewPosition(AZstart * oneTurn / 360);
#endif
#ifdef HAS_EL_STEPPER
        ELstepper.runToNewPosition(0);
#endif
#ifdef DEBUG
        if ((millis() - lastDebug) > 10000)
        {
            lastDebug = millis();
            Serial.println("Pre-pass for : " + String(satname) + " in : " + String(nextpassEpoch - timeNow) + " Az : " + String(AZstart));
        }
#endif

        //satellite visible and tracking
        if ((sat.satVis != -2) && (passStatus == 1))
        {
            // tracking = true;
            trackState = INPASS;
            passEnd = timeNow;
        }

        break;

    case INPASS:
        // Handle zero crossings
#ifdef HAS_AZ_STEPPER
        if (AZstepper.currentPosition() < 0)
            AZstepper.setCurrentPosition(AZstepper.currentPosition() + oneTurn);
        if (AZstepper.currentPosition() > oneTurn)
            AZstepper.setCurrentPosition(AZstepper.currentPosition() - oneTurn);
        if ((AZstepper.currentPosition() > oneTurn * 3 / 4) && (satAZsteps < oneTurn / 4))
            satAZsteps += oneTurn; //zero cross CW
        if ((satAZsteps > oneTurn * 3 / 4) && (AZstepper.currentPosition() < oneTurn / 4))
            AZstepper.setCurrentPosition(AZstepper.currentPosition() + oneTurn); //zero cross CCW
        if (satAZsteps > AZstepper.currentPosition())
            dirCW = true;
        if (satAZsteps < AZstepper.currentPosition())
            dirCW = false;
#endif
        // Update stepper position
        // digitalWrite(ENABLE_PIN, LOW); //enable

        satAZsteps2 = satAZsteps;
        satELsteps2 = satELsteps;
        passStatus = 1;
#ifdef DEBUG_IN_PASS
        if ((millis() - lastDebug) > 5000)
        {
            lastDebug = millis();
            Serial.println("in pass : " + String(satname));
            Serial.println("azimuth = " + String(sat.satAz) + " elevation = " + String(sat.satEl) + " distance = " + String(sat.satDist));
            Serial.println("latitude = " + String(sat.satLat) + " longitude = " + String(sat.satLon) + " altitude = " + String(sat.satAlt));
        }
#endif

        //post pass during 1 minute
        if (timeNow - passEnd < 59)
        {
            // tracking = false;
            trackState = POSTPASS;
        }

        break;

    case POSTPASS:
#ifdef DEBUG
        if ((millis() - lastDebug) > 5000)
        {
            lastDebug = millis();
            Serial.println("Post pass time left: " + String(passEnd + 60 - timeNow));
        }
#endif

        if (timeNow - passEnd < 10) //this is needed to stop the multisteppers at the current postion
        {
#ifdef HAS_AZ_STEPPER
            AZstepper.setCurrentPosition(AZstepper.currentPosition());
#endif

#ifdef HAS_EL_STEPPER
            ELstepper.setCurrentPosition(ELstepper.currentPosition());
#endif
        }
        if (timeNow - passEnd > 20)
        {
            // digitalWrite(ENABLE_PIN, LOW); //enable
            int i = 0;
#ifdef xDEBUG_END_PASS
            Serial.print(String(AZstepper.currentPosition()) + " " + String(posCW) + " " + String(dirCW));
#endif
            //compute way back motion to zero depending on initial position CW or CCW and further direction of motion dir CW or dir CCW
            if (posCW == false) //position CCW
            {
                if (dirCW == true) //motion CW
                {
#ifdef HAS_AZ_STEPPER
                    if (AZstepper.currentPosition() > oneTurn / 2)
                        AZstepper.setCurrentPosition(AZstepper.currentPosition() - oneTurn); //now will go to zero CW
#endif
                    i = 0;
                }
                else
                    i = oneTurn; //motion CCW
            }
            else //position CW ==true
            {
                if (dirCW == true) //motion CW
                {
                    i = 0;
                }
                else //motion CCW
                {
                    if (AZstepper.currentPosition() > oneTurn / 2)
                        AZstepper.setCurrentPosition(AZstepper.currentPosition() - oneTurn); //now will go to zero CW
                    i = 0;
                }
            }
#ifdef xDEBUG_END_PASS
            Serial.println(" " + String(i) + " " + String(AZstepper.currentPosition()));
#endif
#ifdef HAS_AZ_STEPPER
            AZstepper.runToNewPosition(i);
#endif
#ifdef HAS_EL_STEPPER
            ELstepper.runToNewPosition(0); //Standby at 0 degrees above horizon
#endif
        }

        //end pass during 1 s
        if (timeNow - passEnd < 60)
        {
#ifdef DEBUG
            Serial.println("Satellite stopped passing");
#endif
            // tracking = false;
            trackState = ENDPASS;
        }

        break;

    case ENDPASS:
        // TODO: Move back to zero position after each pass
        
        delayNext = 0;                  //goes back to real time
        timeNow = unix_time_get_time(); //update time
        updateSatellites();
        AZstepper.setCurrentPosition(0);
        passStatus = 0;
        tracking = false;
        delay(1000);

        //stand by 1 min after pass and sat not visible
        if (sat.satVis == -2)
        {
#ifdef xDEBUG
            Serial.println("Standby for next pass");
#endif
            // tracking = false;
            trackState = STANDBY;
        }
        break;
    }
}
