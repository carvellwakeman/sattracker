#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <TimeLib.h>
#include <WiFiUdp.h>
#include <math.h>

//SGP4
#include "src/Sgp4-Library/Sgp4.h"

#include <AccelStepper.h> //to drive each stepper
#include <MultiStepper.h> //to synchronize both steppers

// secrets.h should include these two lines:
// #define SSID "your ssid here" and
// #define SSID_PASSWORD "your ssid password here"
#include "secrets.h"

// User setup
#include "preferences.h"

// Debugging
#define DEBUG
#define DEBUG_TLE
// #define DEBUG_SGP4
// #define DEBUG_UPDATE_SAT
// #define DEBUG_IN_PASS
// #define DEBUG_END_PASS
// #define xDEBUG

// Physical hardware
#define HAS_AZ_STEPPER
// #define HAS_EL_STEPPER
#define HAS_HALL_SENSORS //comment this line if no hall sensors

// The Azimuth Stepper pins
#define AZ_DIR_PIN 16 // D0
#define AZ_STEP_PIN 5 // D1
// The Elevation Stepper pins
#define EL_DIR_PIN 0  // D3
#define EL_STEP_PIN 4 // D2
#define ENABLE_PIN 4  //enable pin for both easysteppers boards
// The Hall Sensor pins
#define AZ_ZERO_PIN 14 //azimuth hall sensor D5
#define EL_ZERO_PIN 2  //elevation hall sensor D4

enum SystemState
{
    CALIBRATING,
    READY,
    GET_TLE
};

#ifdef HAS_HALL_SENSORS
int calStatus = CALIBRATING; //when  hall sensors
#else
int calStatus = READY; //when  no hall sensors
#endif
long calTime;

// Define some steppers and the pins the will use
#ifdef HAS_AZ_STEPPER
AccelStepper AZstepper(AccelStepper::DRIVER, AZ_STEP_PIN, AZ_DIR_PIN);
#endif
#ifdef HAS_EL_STEPPER
AccelStepper ELstepper(AccelStepper::DRIVER, EL_STEP_PIN, EL_DIR_PIN);
#endif

// Up to 10 steppers can be handled as a group by MultiStepper
MultiStepper steppers;
long positions[2]; // Array of desired stepper positions for multistepper
int satAZsteps;
int satELsteps;
int satAZsteps2;
int satELsteps2;
long oneTurn = 16384; // Number of steps per one rotation for stepper motor.
boolean tracking = false;

//SGP4
String satname; //Names of satellites. (found here : https://www.celestrak.com/satcat/search.php)
char TLE1[70];
char TLE2[70];

int delayNext = 0; //used to delay acquisition
long TLEtimeOut;
int prevCalStatus;
WiFiClient wifiClient; //used to get TLE

Sgp4 sat;

int AZstart;
long passEnd;
int passStatus = 0;
int years;
int months;
int days;
int hours;
int minutes;
double seconds;
long nextpassEpoch;
long upcomingPasses[4];

// time
int TLEday, nowDay;
unsigned long timeNow = 0;
double jTimeNow; //julian date with ms precision

long lastDebug;

int satID = DEFAULT_SATELLITE_ID; //ID of Celestrak TLEs for satellites.

void moveMotors()
{
    // for (;;)
    // {
    if (tracking)
    {
        // Adjust target position to be within range [0, oneTurn]
        while (positions[0] > oneTurn || positions[0] < 0)
        {
            if (positions[0] > oneTurn)
            {
                positions[0] -= oneTurn;
            }
            else if (positions[0] < 0)
            {
                positions[0] += oneTurn;
            }
            Serial.println("Anti-Twist - adjusting target position to: " + String(positions[0]));
        }

        // TODO: Why delay?
        delay(2);

        //      AZstepper.runToNewPosition(satAZsteps2);
        //      ELstepper.runToNewPosition(satELsteps2);

#ifdef HAS_AZ_STEPPER
#ifdef HAS_EL_STEPPER
        positions[0] = satAZsteps2;
        positions[1] = satELsteps2;
#else
        positions[0] = satAZsteps2;
#endif
#endif

#ifdef HAS_EL_STEPPER
#ifdef HAS_AZ_STEPPER
        positions[0] = satAZsteps2;
        positions[1] = satELsteps2;
#else
        positions[0] = satELsteps2;
#endif
#endif

        steppers.moveTo(positions);
        steppers.runSpeedToPosition(); // Blocks until all are in position
    }
    // else
    // delay(200);
}

void setup()
{
    // pinMode(ENABLE_PIN, OUTPUT);
    // digitalWrite(ENABLE_PIN, HIGH); //disable
    pinMode(AZ_ZERO_PIN, INPUT_PULLUP);
    pinMode(EL_ZERO_PIN, INPUT_PULLUP);

    // wait for serial port to connect. Needed for native USB
    Serial.begin(115200);
    while (!Serial)
    {
        ;
    }

    // Connect to WiFi
    Serial.println();
    Serial.print("WiFi");
    WiFi.begin(SSID, SSID_PASSWORD);
    long start = millis();
    while ((WiFi.status() != WL_CONNECTED) && (millis() - start < 15000))
    {
        delay(500);
        Serial.print(".");
    }
    Serial.print("connected (");
    Serial.print(WiFi.localIP());
    Serial.println(").");

    // Get time
    getTime();

    //SGP4 init
    sat.site(USER_LATITUDE, USER_LONGITUDE, USER_ALTITUDE); //set location latitude[°], longitude[°] and altitude[m]

    // Get TLE
    getTLE();

    // Predict satellite passes from TLEs
    updateSatellites();

    // Setup stepper movements //
    // Then give them to MultiStepper to manage
    // digitalWrite(ENABLE_PIN, HIGH); //disable
#ifdef HAS_EL_STEPPER
    ELstepper.setMaxSpeed(2000);
    ELstepper.setCurrentPosition(0); // Elevation stepper starts at 0 degrees above horizon
    ELstepper.setAcceleration(40.0);
    ELstepper.setPinsInverted(true, false, false); //DIR pin inverted on EL motor --> uncomment to inverse EL motor
    steppers.addStepper(ELstepper);
#endif
#ifdef HAS_AZ_STEPPER
    AZstepper.setMaxSpeed(2000);
    AZstepper.setCurrentPosition(0); // Azimuth stepper starts at 0 (north)
    AZstepper.setAcceleration(40.0);
    //AZstepper.setPinsInverted(true,false,false ); //DIR pin inverted on AZ motor --> uncomment to inverse AZ motor
    steppers.addStepper(AZstepper);
#endif

    lastDebug = millis();
}

void loop()
{
    // Load a satellite via serial input
#ifdef DEBUG
    if (Serial.available() > 0)
    {
        int newSat = Serial.parseInt();
        if (newSat > 0)
        {
            satID = newSat;

            // Predict satellite passes from TLEs
            getTLE();
            updateSatellites();
        }
    }
#endif

    timeNow = unix_time_get_time(); //update time
    switch (calStatus)
    {
    case CALIBRATING:
        if (Calibrate())
        {
            calStatus = READY;
        }
        break;

    case GET_TLE:
        if ((millis() - TLEtimeOut) > 4000) //wait 2s to ingest the TLE
        {
            calStatus = prevCalStatus; //go back to previous state
        }
        break;

    case READY:
        // will update following sat. variables. Pass a double to allow ms accuracy
        sat.findsat(jTimeNow);
        //  satLat  //Latidude satellite (degrees)
        //  satLon //longitude satellite (degrees)
        //  satAlt  //Altitude satellite (degrees)
        //  satAz  //Azimuth satellite (degrees)
        //  satEl //elevation satellite (degrees)
        //  satDist  //Distance to satellite (km)
        //  satJd  //time (julian day)
        // Convert degrees to stepper steps
        satAZsteps = round(sat.satAz * oneTurn / 360);
        satELsteps = round(sat.satEl * oneTurn / 360);

#ifdef xDEBUG
        Serial.println("Satellite Position:");
        Serial.println(sat.satAz);
        Serial.println(sat.satEl);

        invjday(sat.satJd, TIMEZONE, true, years, months, days, hours, minutes, seconds);
        Serial.println("\nLocal time: " + String(days) + '/' + String(months) + '/' + String(years) + ' ' + String(hours) + ':' + String(minutes) + ':' + String(seconds));
        Serial.println("azimuth = " + String(sat.satAz) + " elevation = " + String(sat.satEl) + " distance = " + String(sat.satDist));
        Serial.println("latitude = " + String(sat.satLat) + " longitude = " + String(sat.satLon) + " altitude = " + String(sat.satAlt));
#endif

        // Do tracking work
        track();

        // Update TLE & Unix time everyday.//
        time_t now;
        struct tm *timeinfo;
        time(&now);
        timeinfo = localtime(&now);
        nowDay = timeinfo->tm_mday;

        if (passStatus == 0 && nowDay != TLEday)
        {
            if ((WiFi.status() == WL_CONNECTED) && (timeinfo->tm_hour == 3))
            {
                Serial.println("Restarting ESP to refresh time and TLEs");
                ESP.restart(); //@3 am next day, if no pass then ESP will retart to refresh time and TLEs
            }
        }
        break;
    }

    // Move motors
    moveMotors();
}

void updateSatellites()
{
    if (calStatus != GET_TLE)
        prevCalStatus = calStatus; //then first TLE has been sent, more will come

    calStatus = GET_TLE;
    TLEtimeOut = millis(); //reset the TLE timeout used to escape from GET_TLE state

    sat.init(satname.c_str(), TLE1, TLE2);
    sat.findsat(timeNow);
    upcomingPasses[0] = Predict(1);
}
