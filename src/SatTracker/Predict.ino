// Adapted from sgp4 library
// Predicts time of next pass and start azimuth for satellites
long Predict(int orbits)
{
  passinfo overpass;               //structure to store overpass info
  sat.initpredpoint(timeNow, 0.0); //finds the startpoint

  bool error;
  for (int i = 0; i < orbits; i++)
  {
    error = sat.nextpass(&overpass, 20, false); //search for the next overpass, if there are more than 20 maximums below the horizon it returns false
    delay(0);

    if (error == 1)
    {                                                         //no error, prints overpass information
      nextpassEpoch = (overpass.jdstart - 2440587.5) * 86400; //2440587.5 is the julian day at 1/1/1970 0:00 UTC
      AZstart = overpass.azstart;
      invjday(overpass.jdstart, TIMEZONE, true, years, months, days, hours, minutes, seconds); // Convert Julian date to print in serial.

#ifdef DEBUG
      Serial.println("Next pass for: " + String(satname) + " at " + String(nextpassEpoch) + " (in " + String(nextpassEpoch - timeNow) + "sec)");
      Serial.println("Start: az=" + String(overpass.azstart) + "° " + String(hours) + ':' + String(minutes) + ':' + String(seconds) + "\n");
#endif
    }
    else
    {
      Serial.println("Prediction error");
    }
    delay(0);
  }
  return nextpassEpoch;
}

void Predict_Print(int many)
{
  passinfo overpass; //structure to store overpass info

  sat.initpredpoint(timeNow, 0.0); //finds the startpoint

  bool error;
  unsigned long start = millis();
  for (int i = 0; i < many; i++)
  {
    error = sat.nextpass(&overpass, 20, false); //search for the next overpass, if there are more than 20 maximums below the horizon it returns false
    delay(0);

    if (error == 1)
    { //no error, prints overpass information

      invjday(overpass.jdstart, TIMEZONE, true, years, months, days, hours, minutes, seconds);
      Serial.println("Overpass " + String(days) + ' ' + String(months) + ' ' + String(years));
      Serial.println("  Start: az=" + String(overpass.azstart) + "° " + String(hours) + ':' + String(minutes) + ':' + String(seconds));

      invjday(overpass.jdmax, TIMEZONE, true, years, months, days, hours, minutes, seconds);
      Serial.println("  Max: elev=" + String(overpass.maxelevation) + "° " + String(hours) + ':' + String(minutes) + ':' + String(seconds));

      invjday(overpass.jdstop, TIMEZONE, true, years, months, days, hours, minutes, seconds);
      Serial.println("  Stop: az=" + String(overpass.azstop) + "° " + String(hours) + ':' + String(minutes) + ':' + String(seconds));

      switch (overpass.transit)
      {
      case none:
        break;
      case enter:
        invjday(overpass.jdtransit, TIMEZONE, true, years, months, days, hours, minutes, seconds);
        Serial.println("  Enter earth shadow: " + String(hours) + ':' + String(minutes) + ':' + String(seconds));
        break;
      case leave:
        invjday(overpass.jdtransit, TIMEZONE, true, years, months, days, hours, minutes, seconds);
        Serial.println("  Leave earth shadow: " + String(hours) + ':' + String(minutes) + ':' + String(seconds));
        break;
      }
      switch (overpass.sight)
      {
      case lighted:
        Serial.println("  Visible");
        break;
      case eclipsed:
        Serial.println("  Not visible");
        break;
      case daylight:
        Serial.println("  Daylight");
        break;
      }
    }
    else
    {
      Serial.println("Prediction error");
    }
    delay(0);
  }
}
