#define TIMEZONE 8
#define DAYLIGHTSAVINGS 0

#define USER_LATITUDE 45.465451
#define USER_LONGITUDE -122.584978
#define USER_ALTITUDE 70

#define DEFAULT_SATELLITE_ID 25544
