#include "Arduino/ArduinoWrapper.h"
// #include "tests/MockArduinoWrapper.h"

#include "Satellite.h"
#include "preferences.h"
#include "TimeService.h"

Sgp4 sat;
Satellite::SatellitePosition satPos;

int AZstart;
int AZend;
int years;
int months;
int days;
int hours;
int minutes;
double seconds;
unsigned long nextpassStart;
unsigned long nextpassEnd;

void Satellite::Init()
{
  //SGP4 init
  sat.site(USER_LATITUDE, USER_LONGITUDE, USER_ALTITUDE); //set location latitude[°], longitude[°] and altitude[m]
  Serial.println("[Satellite] Location initialized at " + String(USER_LATITUDE) + " lat ," + String(USER_LONGITUDE) + " lng");
}

void Satellite::UpdatePosition()
{

#ifdef xDEBUG
  Serial.println("[Satellite] Satellite Position:");
  Serial.println(sat.satAz);
  Serial.println(sat.satEl);

  invjday(sat.satJd, TIMEZONE, true, years, months, days, hours, minutes, seconds);
  Serial.println("[Satellite] \nLocal time: " + String(days) + '/' + String(months) + '/' + String(years) + ' ' + String(hours) + ':' + String(minutes) + ':' + String(seconds));
  Serial.println("[Satellite] azimuth = " + String(sat.satAz) + " elevation = " + String(sat.satEl) + " distance = " + String(sat.satDist));
  Serial.println("[Satellite] latitude = " + String(sat.satLat) + " longitude = " + String(sat.satLon) + " altitude = " + String(sat.satAlt));
#endif

  // will update following sat. variables. Pass a double to allow ms accuracy
  sat.findsat(TimeService::Now());
  //  satLat  //Latidude satellite (degrees)
  //  satLon //longitude satellite (degrees)
  //  satAlt  //Altitude satellite (degrees)
  //  satAz  //Azimuth satellite (degrees)
  //  satEl //elevation satellite (degrees)
  //  satDist  //Distance to satellite (km)
  //  satJd  //time (julian day)

  satPos.az = sat.satAz;
  satPos.el = sat.satEl;
  satPos.al = sat.satAlt;
  satPos.dist = sat.satDist;

  satPos.lat = sat.satLat;
  satPos.lng = sat.satLon;
}

unsigned long Satellite::TimeToNextPassStart()
{
  return nextpassStart - TimeService::Now();
}

unsigned long Satellite::TimeToNextPassEnd()
{
  return nextpassEnd;
}

int Satellite::NextPassAz()
{
  return AZstart;
}

int Satellite::NextPassEl()
{
  return 0;
}

int Satellite::GetAzimuth()
{
  return satPos.az;
}
int Satellite::GetElevation()
{
  return satPos.el;
}
int Satellite::GetAltitude()
{
  return satPos.al;
}
double Satellite::GetDistance()
{
  return satPos.dist;
}
double Satellite::GetLatitude()
{
  return satPos.lat;
}
double Satellite::GetLongitude()
{
  return satPos.lng;
}

bool Satellite::Visible()
{
  return sat.satVis != -2;
}

// Adapted from sgp4 library
// Predicts time of next pass and start azimuth for satellites
void Satellite::Predict(int orbits, const char *satName, char tle1[70], char tle2[70])
{
  sat.init(satName, tle1, tle2);
  sat.findsat(TimeService::Now());

  passinfo overpass;                          //structure to store overpass info
  sat.initpredpoint(TimeService::Now(), START_ELEVATION); //finds the startpoint

  bool error;
  for (int i = 0; i < orbits; i++)
  {
    error = sat.nextpass(&overpass, 20, false); //search for the next overpass, if there are more than 20 maximums below the horizon it returns false
    delay(0);

    //no error, prints overpass information
    if (error == 1)
    {
      // Seconds from Unix Epoch until satellite pass
      nextpassStart = (overpass.jdstart - 2440587.5) * 86400; //2440587.5 is the julian day at 1/1/1970 0:00 UTC
      nextpassEnd = (overpass.jdstop - 2440587.5) * 86400; //2440587.5 is the julian day at 1/1/1970 0:00 UTC
      AZstart = overpass.azstart;
      AZend = overpass.azstop;
      invjday(overpass.jdstart, TIMEZONE, DAYLIGHTSAVINGS, years, months, days, hours, minutes, seconds); // Convert Julian date to print in serial.

#ifdef DEBUG
      Serial.println("[Satellite] Next pass for: " + String(satName) + " at " + String(nextpassStart) + " utc (in " + String(nextpassStart - TimeService::Now()) + "sec)");
      Serial.println("[Satellite] Start: az=" + String(overpass.azstart) + "° " + String(hours) + ':' + String(minutes) + ':' + String(seconds) + "\n");
#endif
    }
    else
    {
      Serial.println("[Satellite] Prediction error");
    }
    delay(0);
  }
}
