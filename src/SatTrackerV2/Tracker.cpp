#include "Arduino/ArduinoWrapper.h"
// #include "tests/MockArduinoWrapper.h"

#include "enum.h"
#include "State.h"
#include "TimeService.h"
#include "Satellite.h"
#include "MotorHandler.h"
#include "TLEService.h"
#include "preferences.h"

#include "Tracker.h"

long lastDebug;

unsigned long _timeNow;
unsigned long passStart;
unsigned long passEnd;

bool Tracker::Track()
{
    _timeNow = TimeService::Now();

    switch (State::GetTrackingState())
    {

    // Wait until 1 minute before pass starts
    case STANDBY:
        // digitalWrite(ENABLE_PIN, HIGH); //disable

#ifdef DEBUG
        if ((millis() - lastDebug) > 30000)
        {
            lastDebug = millis();
            Serial.println("[Tracker] Standby. " + String(TLEService::GetSatName()) + " will pass in: " + String(Satellite::TimeToNextPassStart()) + " sec");
        }
#endif

        //one minute before pass (and less than 5s after start)
        passStart = Satellite::TimeToNextPassStart();
        passEnd = Satellite::TimeToNextPassEnd();
        if (passStart < PRE_PASS_START_SEC && passStart + 5 > 0)
        {
            Serial.println("[Tracker] Switching to PrePass for " + String(TLEService::GetSatName()));
            MotorHandler::SetAngle(Satellite::NextPassAz(), Satellite::NextPassEl());
            State::SetTrackingState(TrackState::PREPASS);
        }

        break;

    // Move to position where satellite will start passing
    case PREPASS:
        MotorHandler::SetAngle(Satellite::NextPassAz(), Satellite::NextPassEl());

#ifdef DEBUG
        if ((millis() - lastDebug) > 10000)
        {
            lastDebug = millis();
            Serial.println("[Tracker] Pre-pass for: " + String(TLEService::GetSatName()) + " in : " + String(Satellite::TimeToNextPassStart()) + ", Start Az : " + String(Satellite::NextPassAz()));
        }
#endif

        // Satellite is visible and tracking (in pass)
        if (Satellite::Visible())
        {
            Serial.println("[Tracker] Satellite Visible. Switching to In Pass for " + String(TLEService::GetSatName()) + " to last " + String(passEnd - passStart) + " seconds");
            State::SetTrackingState(TrackState::INPASS);
        }

        break;

    case INPASS:
        MotorHandler::SetAngle(Satellite::GetAzimuth(), Satellite::GetElevation());

        // digitalWrite(ENABLE_PIN, LOW); //enable

#ifdef DEBUG_IN_PASS
        if ((millis() - lastDebug) > 5000)
        {
            lastDebug = millis();
            Serial.println("in pass : " + String(TLEService::GetSatName()));
            Serial.println("azimuth = " + String(Satellite::GetAzimuth()) + " elevation = " + String(Satellite::GetElevation()) + " distance = " + String(Satellite::GetDistance()));
            Serial.println("latitude = " + String(Satellite::GetLatitude()) + " longitude = " + String(Satellite::GetLongitude()) + " altitude = " + String(Satellite::GetAltitude()));
            Serial.println("pass end = " + String(passEnd) + " in = " + String(passEnd - _timeNow));
        }
#endif

        // Predicted end of pass elapsed
        if (passEnd - _timeNow < 0)
        {
            Serial.println("[Tracker] Switching to End Pass for " + String(TLEService::GetSatName()));

            State::SetTrackingState(TrackState::POSTPASS);
        }

        break;

    // Wait for 30 seconds after predicted end pass
    case POSTPASS:
#ifdef DEBUG
        if ((millis() - lastDebug) > 5000)
        {
            lastDebug = millis();
            Serial.println("[Tracker] Post pass time left: " + String(passEnd + 30 - _timeNow));
        }
#endif

        if (_timeNow - passEnd >= 30)
        {
            Serial.println("[Tracker] Switching to End Pass for " + String(TLEService::GetSatName()));

            State::SetTrackingState(TrackState::POSTPASS);
        }
        break;

    // Begin return trip home once satellite is below horizon
    case ENDPASS:
        if (!Satellite::Visible())
        {
            MotorHandler::SetAngle(0, 0);

#ifdef DEBUG
            Serial.println("[Tracker] Returning to Home position for next pass");
#endif

            State::SetTrackingState(TrackState::RETURN_HOME);
        }
        break;

    case RETURN_HOME:
        if (MotorHandler::ReachedTarget())
        {
            Serial.println("[Tracker] Switching to Standby until next pass");

            State::SetTrackingState(TrackState::STANDBY);

            return true;
        }
    }

    return false;
}
