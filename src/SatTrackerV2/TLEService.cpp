#include "Arduino/ArduinoWrapper.h"
// #include "tests/MockArduinoWrapper.h"

#include "TLEService.h"

#include "preferences.h"

//Example: https://www.celestrak.com/satcat/tle.php?CATNR=39019
char server[] = "http://www.celestrak.com"; //Web address to get TLE (CELESTRAK) : //(https://www.celestrak.com/satcat/tle.php?CATNR=39019)
String startURL = "/satcat/tle.php?CATNR="; // https://www.celestrak.com/satcat/tle.php?CATNR=

/// HTTP client errors (https://github.com/esp8266/Arduino/blob/master/libraries/ESP8266HTTPClient/src/ESP8266HTTPClient.h)
#define HTTPC_ERROR_CONNECTION_FAILED (-1)
#define HTTPC_ERROR_SEND_HEADER_FAILED (-2)
#define HTTPC_ERROR_SEND_PAYLOAD_FAILED (-3)
#define HTTPC_ERROR_NOT_CONNECTED (-4)
#define HTTPC_ERROR_CONNECTION_LOST (-5)
#define HTTPC_ERROR_NO_STREAM (-6)
#define HTTPC_ERROR_NO_HTTP_SERVER (-7)
#define HTTPC_ERROR_TOO_LESS_RAM (-8)
#define HTTPC_ERROR_ENCODING (-9)
#define HTTPC_ERROR_STREAM_WRITE (-10)
#define HTTPC_ERROR_READ_TIMEOUT (-11)

String responseBody; //Variable to store satellite TLEs.

//boot with no TLE and try to get them either via Wifi (celestrack)
bool noTLE = true;

//SGP4
String satName; //Names of satellites. (found here : https://www.celestrak.com/satcat/search.php)
char TLE1[70];
char TLE2[70];

HTTPClient _httpClient;

void TLEService::Init()
{
    Serial.println("[TLE] Service initialized.");
}

bool TLEService::GetTLEInfo(WiFiClient wifiClient, int satId)
{
    Serial.println("[TLE] Getting satellite " + String(satId));

    // Make HTTP request
    String requestUrl = server + startURL + String(satId);
#ifdef DEBUG_TLE
    Serial.print("[TLE] Satellite " + String(satId) + " - GET " + requestUrl + " - ");
#endif
    _httpClient.begin(wifiClient, requestUrl);

    int statusCode = _httpClient.GET();

// Errors are < 0
#ifdef DEBUG_TLE
    Serial.println(statusCode);
#endif

    if (statusCode > 0)
    {
        responseBody = _httpClient.getString();

#ifdef DEBUG_TLE
        Serial.println(responseBody);
#endif
    }
    else
    {
#ifdef DEBUG_TLE
        Serial.println("Error on HTTP request: " + String(statusCode));
#endif
        // Free resources
        _httpClient.end();
    
        return false;
    }

    // Free resources
    _httpClient.end();

    // Process TLE data
    int connectLoop = 0;
    int j = 0;
    int idx1, idx2, idx3;
    String temp;

    // Example response body
    // ISS (ZARYA)
    // 1 25544U 98067A   16065.25775256 -.00164574  00000-0 -25195-2 0  9990
    // 2 25544  51.6436 216.3171 0002750 185.0333 238.0864 15.54246933988812

    // TLE name, line 1, spans characters 1 - 25
    idx1 = responseBody.indexOf('\n');
    satName = responseBody.substring(0, idx1 - 1);
    satName.trim();

    // TODO: Rebuild to split by newline
    // TLE line 1 spans characters 26 - 96
    idx2 = responseBody.indexOf('\n', idx1 + 1);
    temp = responseBody.substring(idx1 + 1, idx2 - 1);
    temp.toCharArray(TLE1, temp.length() + 1);

    // TLE line 2 spans characters 97 - 167
    idx3 = responseBody.indexOf('\n', idx2 + 1);
    temp = responseBody.substring(idx2 + 1, idx3 - 1);
    temp.toCharArray(TLE2, temp.length() + 1);

    noTLE = false;

    return true;
}

const char *TLEService::GetSatName()
{
    return satName.c_str();
}

char *TLEService::GetTLE1()
{
    return TLE1;
}

char *TLEService::GetTLE2()
{
    return TLE2;
}
