#ifndef _ARDUINO_DEPS_
#define _ARDUINO_DEPS_

#include <ESP8266WiFi.h>
#include <ESP8266HTTPClient.h>
#include <WiFiUdp.h>
#include <math.h>
#include <Arduino.h>
#include <string.h>
#include <TimeLib.h>
#include <AccelStepper.h> //to drive each stepper
#include <MultiStepper.h> //to synchronize both steppers

//SGP4
#include "../src/Sgp4-Library/Sgp4.h"

#endif