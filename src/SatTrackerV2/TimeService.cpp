#include "preferences.h"

#include "TimeService.h"

//boot with no time and try to get it via wifi (NTP)
bool noTime = true;

unsigned long skipTime = 0; //used to delay acquisition

// time
struct tm *timeinfo;
// int TLEday, nowDay;
unsigned long timeNow = 0;
// double jTimeNow; //julian date with ms precision

void TimeService::Init()
{
    // TODO: Why get time twice?
    //init and get the time
    Serial.println("Trying to get time 1");
    configTime(TIMEZONE * 3600, DAYLIGHTSAVINGS, "pool.ntp.org", "time.nist.gov");

    //init and get the time
    Serial.println("Trying to get time 2"); //call it twice to have a well synchronized time on soft reset... Why ? bex=caus eit works...
    delay(2000);
    configTime(TIMEZONE * 3600, DAYLIGHTSAVINGS, "pool.ntp.org", "time.nist.gov");
    noTime = false;

    // Need time to continue
    if (noTime)
    {
        Serial.println("Could not get time, pausing forever");
        while (true)
        {
        }
    }

    TimeService::Update();
    Serial.println("Time is (epoch) " + String(timeNow));
}

void TimeService::Update()
{
    time_t now;
    time(&now);
    timeinfo = localtime(&now);

    struct timeval tv;
    static long delta = 0;
    gettimeofday(&tv, NULL);

    delta = 0;

    // Convert from unix time to julian time
    timeNow = (tv.tv_sec + skipTime);
    // jTimeNow = (double((timeNow * 1000LL + (tv.tv_usec / 1000LL))) / 86400.0 / 1000.) + 2440587.5;
#ifdef XDEBUG
    Serial.println("Time Updated. Now is " + String(timeNow));
#endif
}

int TimeService::GetDay()
{
    return timeinfo->tm_hour;
}

int TimeService::GetHourOfDay()
{
    return timeinfo->tm_mday;
}

unsigned long TimeService::Now()
{
    return timeNow;
}

void TimeService::SetSkipTimeSeconds(unsigned long seconds)
{
    skipTime = seconds;
}
