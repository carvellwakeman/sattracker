#include "Arduino/ArduinoWrapper.h"

// Defined structures
#include "enum.h"
#include "State.h"
#include "Tracker.h"
#include "Satellite.h"
#include "TLEService.h"
#include "TimeService.h"
#include "MotorHandler.h"

// secrets.h should include these two lines:
// #define SSID "your ssid here" and
// #define SSID_PASSWORD "your ssid password here"
#include "secrets.h"

// User setup
#include "preferences.h"

long TLEtimeOut;
SystemState prevsystemState;
WiFiClient wifiClient; //used to get TLE

int satID = DEFAULT_SATELLITE_ID; //ID of Celestrak TLEs for satellites.

void setup()
{
    // State init
#ifdef HAS_HALL_SENSORS
    State::SetSystemState(SystemState::CALIBRATING); //when hall sensors
#else
    State::SetSystemState(SystemState::READY); //when no hall sensors
#endif

#ifdef SKIP_CALIBRATION
    State::SetCalibrationState(CalibState::DONE);
#else
    State::SetCalibrationState(CalibState::BEGIN_CALIB);
#endif

    State::SetTrackingState(TrackState::STANDBY);

    // pinMode(ENABLE_PIN, OUTPUT);
    // digitalWrite(ENABLE_PIN, HIGH); //disable
    pinMode(AZ_ZERO_PIN, INPUT_PULLUP);
    pinMode(EL_ZERO_PIN, INPUT_PULLUP);

    // wait for serial port to connect. Needed for native USB
    Serial.begin(115200);
    while (!Serial)
    {
        ;
    }

    Serial.println("[SatTracker] Booting...");

    // Connect to WiFi
    Serial.println();
    Serial.print("WiFi");
    WiFi.begin(SSID, SSID_PASSWORD);
    long start = millis();
    while ((WiFi.status() != WL_CONNECTED) && (millis() - start < 15000))
    {
        delay(500);
        Serial.print(".");
    }
    Serial.print("connected (");
    Serial.print(WiFi.localIP());
    Serial.println(").");

    // Update time
    TimeService::Init();

    // Satellite init
    Satellite::Init();

    // TLE service init
    TLEService::Init();

    // Setup stepper movements
    MotorHandler::Init();

    // Get TLE
    TLEService::GetTLEInfo(wifiClient, satID);

    // Predict satellite passes from TLEs
    Satellite::Predict(1, TLEService::GetSatName(), TLEService::GetTLE1(), TLEService::GetTLE2());

    // Wait a bit in case we're uploading a sketch
    start = millis();
    while (millis() - start < 5000)
    {
        delay(50);
    }
}

void loop()
{
    // Load a satellite via serial input
#ifdef DEBUG
    if (Serial.available() > 0)
    {
        char debugParam = Serial.read();

        int newSat;
        String longInput;
        unsigned long parsedLong;
        bool newTleResult;
        switch (debugParam)
        {
        // track new satellite
        case 's':
            newSat = Serial.parseInt();
            if (newSat > 0)
            {
                Serial.println("[DEBUG] Input accepted. Tracking new satellite " + String(newSat));

                // Get new TLE
                newTleResult = TLEService::GetTLEInfo(wifiClient, newSat);
                if (newTleResult)
                {
                    // Predict satellite passes from TLEs
                    Satellite::Predict(1, TLEService::GetSatName(), TLEService::GetTLE1(), TLEService::GetTLE2());
                    satID = newSat;
                }
            }
            break;
        // Skip forward n seconds
        case 'd':
            longInput = Serial.readString();
            parsedLong = strtoul(longInput.c_str(), NULL, 10);
            TimeService::SetSkipTimeSeconds(parsedLong);

            Serial.println("[DEBUG] Input accepted. Skipping forward " + longInput + " seconds");
            break;
        // offset time to pass now
        case 'n':
            parsedLong = Serial.parseInt();

            // Reset our prediction to the current values
            TimeService::SetSkipTimeSeconds(0);
            TimeService::Update();
            Satellite::UpdatePosition();

            // Skip ahead
            TimeService::SetSkipTimeSeconds(Satellite::TimeToNextPassStart() - parsedLong);
            Serial.println("[DEBUG] Input accepted. Skipping forward to " + String(parsedLong) + " seconds before pass");
            break;
        // reset
        case 'r':
            State::SetSystemState(SystemState::READY);
            State::SetCalibrationState(CalibState::DONE);
            MotorHandler::FullStop();
            MotorHandler::SetAngle(0, 0);
            TimeService::SetSkipTimeSeconds(0);
            State::SetTrackingState(TrackState::RETURN_HOME);
            Serial.println("[DEBUG] Input accepted. Resetting.");
            break;
        // Set azimuth angle
        case 'a':
            parsedLong = Serial.parseInt();

            State::SetTrackingState(TrackState::MANUAL_MODE);
            MotorHandler::SetAngle(parsedLong, MotorHandler::GetElAngle());
            Serial.println("[DEBUG] Input accepted. AZ to " + String(parsedLong) + " deg");
            break;
        // Set elevation angle
        case 'e':
            parsedLong = Serial.parseInt();

            State::SetTrackingState(TrackState::MANUAL_MODE);
            MotorHandler::SetAngle(MotorHandler::GetAzAngle(), parsedLong);

            Serial.println("[DEBUG] Input accepted. EL to " + String(parsedLong) + " deg");
            break;
        // Run calibration
        case 'c':
            State::SetCalibrationState(CalibState::BEGIN_CALIB);
            State::SetSystemState(SystemState::CALIBRATING);
            State::SetTrackingState(TrackState::STANDBY);
            Serial.println("[DEBUG] Input accepted. Running Calibration.");
            break;
        }
    }
#endif

    // Update time
    TimeService::Update();

    switch (State::GetSystemState())
    {
    case CALIBRATING:
        if (MotorHandler::Calibrate())
        {
            State::SetSystemState(SystemState::READY);
        }
        break;

        // case GET_TLE:
        //     if ((millis() - TLEtimeOut) > 4000) //wait 2s to ingest the TLE
        //     {
        // State::SetSystemState(prevsystemState);
        //     }
        //     break;

    case READY:
        // Update calculated satellite position
        Satellite::UpdatePosition();

        // Track the satellite
        Tracker::Track();

        // Restart at 3am every day, if not tracking
        if (State::GetTrackingState() == TrackState::STANDBY && (TimeService::GetHourOfDay() == 3))
        {
            if (WiFi.status() == WL_CONNECTED)
            {
                Serial.println("Restarting ESP to refresh time and TLEs");
                ESP.restart(); //@3 am next day, if no pass then ESP will retart to refresh time and TLEs
            }
        }
        break;
    }

    // Move motors
    MotorHandler::MoveMotors();
}
