#ifndef H_MOTORHANDLER
#define H_MOTORHANDLER

namespace MotorHandler
{
    struct MotorPosition
    {
        // Angle 0-359 for Azimuth, Elevation
        int azAng, elAng;
        // Absolute target position for Azimuth, Elevation
        long targetAzSteps, targetElSteps;
        // Relative position for Azimuth, Elevation. Limited to 0-STEPPER_ONE_TURN
        long azSteps, elSteps;

        // Direction modifiers to pass to setSpeed
        int azDir, elDir;
    };

    void Init();
    void MoveMotors();
    void SetAngle(int azAngle, int elAngle);
    int GetAzAngle();
    int GetElAngle();
    MotorPosition GetPosition();
    bool ReachedTarget();
    void FullStop();

    bool Calibrate();
}

#endif