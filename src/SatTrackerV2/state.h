#ifndef H_STATE
#define H_STATE

#include "enum.h"
#include "preferences.h"

namespace State
{
    SystemState GetSystemState();
    void SetSystemState(SystemState newState);

    CalibState GetCalibrationState();
    void SetCalibrationState(CalibState newState);

    TrackState GetTrackingState();
    void SetTrackingState(TrackState newState);
}

#endif
