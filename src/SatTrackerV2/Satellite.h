#ifndef H_SATELLITE
#define H_SATELLITE

namespace Satellite
{
    struct SatellitePosition
    {
        // Azimuth, Elevation, Altitude
        int az, el, al;

        // Latitude, Longitude
        double lat, lng;

        // Distance from us
        double dist;
    };

    void Init();
    void UpdatePosition();
    void Predict(int orbits, const char *satName, char tle1[70], char tle2[70]);

    unsigned long TimeToNextPassStart();
    unsigned long TimeToNextPassEnd();

    int NextPassAz();
    int NextPassEl();

    int GetAzimuth();
    int GetElevation();
    int GetAltitude();
    double GetDistance();
    double GetLatitude();
    double GetLongitude();

    bool Visible();
}

#endif