#ifndef H_PREFERENCES
#define H_PREFERENCES


// Location
#define TIMEZONE -7
#define DAYLIGHTSAVINGS true

#define USER_LATITUDE 45.52
#define USER_LONGITUDE -122.60
#define USER_ALTITUDE 13

// Tracking
#define DEFAULT_SATELLITE_ID 39239
#define START_ELEVATION 0 // Degrees above horizon to start tracking
#define PRE_PASS_START_SEC 120

// Physical hardware
#define HAS_AZ_STEPPER
#define HAS_EL_STEPPER
#define HAS_HALL_SENSORS

#define SKIP_CALIBRATION

// Number of steps in one stepper rotation
#define STEPPER_ONE_TURN 16384
#define EL_STEPPER_MAX_SPEED 2000
#define AZ_STEPPER_MAX_SPEED 2000
#define EL_STEPPER_TRACK_SPEED 500
#define AZ_STEPPER_TRACK_SPEED 500
#define EL_STEPPER_MAX_ANGLE 150

// The Azimuth Stepper pins
#define AZ_DIR_PIN 16 // D0
#define AZ_STEP_PIN 5 // D1
// The Elevation Stepper pins
#define EL_DIR_PIN 0  // D3
#define EL_STEP_PIN 4 // D2
#define ENABLE_PIN 4  //enable pin for both easysteppers boards
// The Hall Sensor pins
#define AZ_ZERO_PIN 14 //azimuth hall sensor D5
#define EL_ZERO_PIN 2  //elevation hall sensor D4


// Debugging
#define DEBUG
#define DEBUG_TLE
// #define DEBUG_SGP4
// #define DEBUG_UPDATE_SAT
// #define DEBUG_IN_PASS
// #define DEBUG_END_PASS
// #define xDEBUG

#endif
