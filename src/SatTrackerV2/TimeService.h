#ifndef H_TIMESERVICE
#define H_TIMESERVICE

#include "Arduino/ArduinoWrapper.h"
// #include "tests/MockArduinoWrapper.h"

namespace TimeService
{
    void Init();
    void Update();
    int GetDay();
    int GetHourOfDay();

    unsigned long Now();
    void SetSkipTimeSeconds(unsigned long seconds);
}

#endif