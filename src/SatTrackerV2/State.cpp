
#include "State.h"
#include "enum.h"
#include "preferences.h"

// Main state
SystemState systemState;

// Calibration state
CalibState calibrationState;

// Tracking state
TrackState trackState;

SystemState State::GetSystemState()
{
    return systemState;
}

void State::SetSystemState(SystemState newState)
{
    systemState = newState;
}

CalibState State::GetCalibrationState()
{
    return calibrationState;
}

void State::SetCalibrationState(CalibState newState)
{
    calibrationState = newState;
}

TrackState State::GetTrackingState()
{
    return trackState;
}

void State::SetTrackingState(TrackState newState)
{
    trackState = newState;
}
