#ifndef H_ENUM
#define H_ENUM

enum SystemState
{
    CALIBRATING,
    READY
    // GET_TLE
};

enum CalibState
{
    BEGIN_CALIB,
    AZ_ONSENSOR,
    AZ_ZERO1,
    AZ_ZERO2a,
    AZ_ZERO2b,
    AZ_ZERO3,
    AZ_ZERO4,
    EL_ZERO1,
    EL_ZERO2,
    EL_ZERO3,
    EL_ZERO4,
    HORIZONTAL,
    DONE
};

enum TrackState
{
    STANDBY,
    PREPASS,
    INPASS,
    POSTPASS,
    ENDPASS,
    RETURN_HOME,
    MANUAL_MODE
};

#endif
