#include "Arduino/ArduinoWrapper.h"
// #include "tests/MockArduinoWrapper.h"

#ifndef H_TLESERVICE
#define H_TLESERVICE

namespace TLEService
{
    void Init();
    bool GetTLEInfo(WiFiClient wifiClient, int satId);
    const char *GetSatName();
    char *GetTLE1();
    char *GetTLE2();
}

#endif