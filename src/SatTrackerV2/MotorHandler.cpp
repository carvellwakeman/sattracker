#include "Arduino/ArduinoWrapper.h"

#include "MotorHandler.h"
#include "enum.h"
#include "State.h"
#include "Tracker.h"

#include "preferences.h"

// Define some steppers and the pins the will use
#ifdef HAS_AZ_STEPPER
AccelStepper AZstepper(AccelStepper::DRIVER, AZ_STEP_PIN, AZ_DIR_PIN);
#endif
#ifdef HAS_EL_STEPPER
AccelStepper ELstepper(AccelStepper::DRIVER, EL_STEP_PIN, EL_DIR_PIN);
#endif

MotorHandler::MotorPosition motorPos;
long positions[2];

// Up to 10 steppers can be handled as a group by MultiStepper
MultiStepper steppers;

int azDist;

void MotorHandler::Init()
{
#ifdef HAS_EL_STEPPER
    ELstepper.setMaxSpeed(EL_STEPPER_MAX_SPEED);
    ELstepper.setCurrentPosition(0); // Elevation stepper starts at 0 degrees above horizon
    // ELstepper.setAcceleration(40.0);
    ELstepper.setPinsInverted(true, false, false); //DIR pin inverted on EL motor --> uncomment to inverse EL motor
    steppers.addStepper(ELstepper);
#endif
#ifdef HAS_AZ_STEPPER
    AZstepper.setMaxSpeed(AZ_STEPPER_MAX_SPEED);
    AZstepper.setCurrentPosition(0); // Azimuth stepper starts at 0 (north)
    // AZstepper.setAcceleration(40.0);
    //AZstepper.setPinsInverted(true,false,false ); //DIR pin inverted on AZ motor --> uncomment to inverse AZ motor
    steppers.addStepper(AZstepper);
#endif

    motorPos.azAng = 0;
    motorPos.elAng = 0;
    motorPos.azSteps = 0;
    motorPos.elSteps = 0;
    motorPos.targetAzSteps = 0;
    motorPos.targetElSteps = 0;

    Serial.println("[MotorHandler] Stepper motors initialized.");
}

void MotorHandler::MoveMotors()
{
    if (State::GetTrackingState() != TrackState::STANDBY)
    {
        // #ifdef DEBUG
        //         Serial.println("[MotorHandler] Distance AZ: " + String(AZstepper.distanceToGo()) + " EL: " + String(ELstepper.distanceToGo()));
        //         Serial.println("[MotorHandler] Moving motor to EL " + String(ELstepper.currentPosition()) + "/" + String(ELstepper.targetPosition()) + " and AZ " + String(AZstepper.currentPosition()) + "/" + String(AZstepper.targetPosition()));
        // #endif

        AZstepper.setSpeed(AZ_STEPPER_MAX_SPEED * motorPos.azDir);
        ELstepper.setSpeed(EL_STEPPER_MAX_SPEED * motorPos.elDir);
        steppers.run();
    }
}

void MotorHandler::SetAngle(int azAngle, int elAngle)
{
    // Anti twist
    while (azAngle >= 360 || azAngle < 0)
    {
        if (azAngle > 360)
        {
            azAngle -= 360;
        }
        else if (azAngle < 0)
        {
            azAngle += 360;
        }
#ifdef DEBUG
        Serial.println("[MotorHandler] Anti-Twist - adjusting target Az angle to: " + String(azAngle));
#endif
    }

    // El limits
    if (elAngle >= EL_STEPPER_MAX_ANGLE)
    {
        elAngle = EL_STEPPER_MAX_ANGLE;
    }
    else if (elAngle < 0)
    {
        elAngle = 0;
    }

    motorPos.azAng = azAngle;
    motorPos.elAng = elAngle;

    // Convert degrees to stepper steps
    motorPos.azSteps = round((azAngle * STEPPER_ONE_TURN) / 360);
    motorPos.elSteps = round((elAngle * STEPPER_ONE_TURN) / 360);

    positions[0] = motorPos.elSteps;
    positions[1] = motorPos.azSteps;
    steppers.moveTo(positions);

    // Determine movement direction
    motorPos.azDir = AZstepper.distanceToGo() > 0 ? 1 : -1;
    motorPos.elDir = ELstepper.distanceToGo() > 0 ? 1 : -1;
}

int MotorHandler::GetAzAngle()
{
    return motorPos.azAng;
}

int MotorHandler::GetElAngle()
{
    return motorPos.elAng;
}

bool MotorHandler::ReachedTarget()
{
    return AZstepper.distanceToGo() == 0 && ELstepper.distanceToGo() == 0;
}

void MotorHandler::FullStop()
{
    // AZstepper.stop();
    // ELstepper.stop();
    AZstepper.moveTo(AZstepper.currentPosition());
    ELstepper.moveTo(ELstepper.currentPosition());
    AZstepper.setSpeed(0);
    ELstepper.setSpeed(0);
    AZstepper.run();
    ELstepper.run();
}

bool MotorHandler::Calibrate()
{
    switch (State::GetCalibrationState())
    {
    // Begin calibration
    case BEGIN_CALIB:
        Serial.println("[MotorHandler] Beginning calibration");

        // digitalWrite(ENABLE_PIN, LOW);        //enable

        AZstepper.setCurrentPosition(0);

        // If dish is already in AZ sensor's range, shuffle left a bit so we can probe the left edge first
        if (digitalRead(AZ_ZERO_PIN) == LOW)
        {
            Serial.println("[MotorHandler] Already on AZ sensor, moving left to clear.");
            AZstepper.move((STEPPER_ONE_TURN * 45 / 360) * -1);

            State::SetCalibrationState(AZ_ONSENSOR);
        }
        else
        {
            State::SetCalibrationState(AZ_ZERO1);
        }

        break;

    // If the sensor is hit initially, backup a bit so we can start on its left edge
    case AZ_ONSENSOR:
        if (AZstepper.distanceToGo() < 0)
        {
            AZstepper.setSpeed(500);
            AZstepper.runSpeedToPosition();
        }
        else
        {
            State::SetCalibrationState(AZ_ZERO1);
        }
        break;

    // Run Az motor CW until hall effect sensor reached
    case AZ_ZERO1:
        if (digitalRead(AZ_ZERO_PIN) == HIGH)
        {
            AZstepper.setSpeed(100);
            AZstepper.runSpeed();
        }
        else
        {
            Serial.println("[MotorHandler] Azimuth sensor reached");

            // If we've moved more than 180deg to find the sensor, we've twisted the wire
            if (AZstepper.currentPosition() >= STEPPER_ONE_TURN * 0.5)
            {
                AZstepper.moveTo(0); // Move backwards around the circle
                AZstepper.setSpeed(-100);

                Serial.println("[MotorHandler] Beginning azimuth second pass (w/ Anti-Twist)");
                State::SetCalibrationState(AZ_ZERO2a);
            }
            else
            {
                AZstepper.setCurrentPosition(0);
                AZstepper.moveTo(STEPPER_ONE_TURN * 45 / 360);
                AZstepper.setSpeed(100);

                Serial.println("[MotorHandler] Beginning azimuth second pass");
                State::SetCalibrationState(AZ_ZERO2b);
            }
        }
        break;

    // Move back to the starting az to avoid twisting wire
    case AZ_ZERO2a:
        if (AZstepper.distanceToGo() > 0)
        {
            AZstepper.runSpeedToPosition();
        }
        else
        {
            AZstepper.setCurrentPosition(0);
            AZstepper.moveTo(STEPPER_ONE_TURN * 45 / 360);
            AZstepper.setSpeed(100);
            State::SetCalibrationState(AZ_ZERO2b);
        }
        break;

    // Move 45 degrees past sensor
    case AZ_ZERO2b:
        if (AZstepper.distanceToGo() > 0)
        {
            AZstepper.runSpeedToPosition();
        }
        else
        {
            State::SetCalibrationState(AZ_ZERO3);
        }
        break;

    // Run Az motor CW until hall effect sensor reached
    case AZ_ZERO3:
        if (digitalRead(AZ_ZERO_PIN) == HIGH)
        {
            AZstepper.setSpeed(-100);
            AZstepper.runSpeed();
        }
        else
        {
            Serial.println("[MotorHandler] Azimuth sensor reached (pass 2)");

            AZstepper.stop(); //magnet reached
            AZstepper.moveTo(AZstepper.currentPosition() / 2);
            State::SetCalibrationState(AZ_ZERO4);
        }
        break;

    // Motor is now at Left side of sensor, with Zero as Right side of sensor
    // Move half of current position towards sensor to find center
    case AZ_ZERO4:
        if (AZstepper.distanceToGo() > 0)
        {
            AZstepper.setSpeed(100);
            AZstepper.runSpeedToPosition();
        }
        else
        {
            Serial.println("[MotorHandler] Azimuth motor calibrated");
            Serial.println("[MotorHandler] Beginning Elevation motor calibration");

            AZstepper.setCurrentPosition(0);

            State::SetCalibrationState(EL_ZERO1);
        }
        break;

    // Run Elevation motor CW until first edge of hall sensor reached
    case EL_ZERO1:
        if (digitalRead(EL_ZERO_PIN) == HIGH)
        {
            ELstepper.setSpeed(100);
            ELstepper.runSpeed();
        }
        else
        {
            Serial.println("[MotorHandler] Elevation sensor reached");
            Serial.println("[MotorHandler] Beginning elevation second pass");

            ELstepper.setCurrentPosition(0);
            ELstepper.moveTo(STEPPER_ONE_TURN * 45 / 360);
            ELstepper.setSpeed(100);

            State::SetCalibrationState(EL_ZERO2);
        }
        break;

    // Move 45 degrees past sensor
    case EL_ZERO2:
        if (ELstepper.distanceToGo() != 0)
        {
            ELstepper.runSpeedToPosition();
        }
        else
        {
            State::SetCalibrationState(EL_ZERO3);
        }
        break;

    // Run El motor CW until hall effect sensor reached
    case EL_ZERO3:
        if (digitalRead(EL_ZERO_PIN) == HIGH)
        {
            ELstepper.setSpeed(-100);
            ELstepper.runSpeed();
        }
        else
        {
            Serial.println("[MotorHandler] Elevation sensor reached (pass 2)");

            ELstepper.stop(); //magnet reached
            ELstepper.moveTo(ELstepper.currentPosition() / 2);

            State::SetCalibrationState(EL_ZERO4);
        }
        break;

    // Move to halfway between both edges of the sensor
    case EL_ZERO4:
        if (ELstepper.distanceToGo() != 0)
        {
            ELstepper.setSpeed(100);
            ELstepper.runSpeedToPosition();
        }
        else
        {
            Serial.println("[MotorHandler] Elevation motor calibrated");

            ELstepper.setCurrentPosition(STEPPER_ONE_TURN * 90 / 360);
            ELstepper.moveTo(0);

            State::SetCalibrationState(HORIZONTAL);
        }
        break;

    // Move antenna to point straight forward
    case HORIZONTAL:
        if (ELstepper.distanceToGo() != 0)
        {
            ELstepper.setSpeed(100);
            ELstepper.runSpeedToPosition();
        }
        else
        {
            Serial.println("[MotorHandler] Antenna horizontal");
            State::SetCalibrationState(DONE);
        }
        break;

    // Find north (TODO: No compass sensor)
    case DONE:
        Serial.println("[MotorHandler] Antenna pointing North");
        Serial.println("[MotorHandler] Calibration Complete");
        AZstepper.setCurrentPosition(0);
        ELstepper.setCurrentPosition(0);

        motorPos.azSteps = 0;
        motorPos.elSteps = 0;
        motorPos.targetElSteps = 0;
        motorPos.targetElSteps = 0;
        motorPos.azAng = 0;
        motorPos.elAng = 0;
        return true;
    }

    return false;
}