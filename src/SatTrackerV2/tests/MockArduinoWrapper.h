#define _ARDUINO_DEPS_


//SGP4
#include "../src/Sgp4-Library/Sgp4.h"


// This header defines non-functional classes and methods from the Arduino libraries in ArduinoWrapper.h for mocking & testing

// <Arduino.h>
// Replace Serial.begin, Serial.print, Serial.println
static class
{
public:
    void begin(...) {}
    void print(...) {}
    void println(...) {}
} Serial;

// Replace delay()
static void delay(...);

// Replace millis()
static long millis(...);

// <TimeLib.h>
// typedef unsigned int time_t;

struct tm
{
    int tm_sec;
    int tm_min;
    int tm_hour;
    int tm_mday;
    int tm_mon;
    int tm_year;
    int tm_wday;
    int tm_yday;
    int tm_isdst;
    long int tm_gmtoff;
    const char *tm_zone;
};

struct timeval
{
    int tv_sec;
    int tv_usec;
};

static void configTime(...);
static void time(...);
static struct tm *localtime(...);

static void gettimeofday(...);

// <String.h>
class String
{
public:
    String(...)
    {
    }

    String &operator+=(const String &rhs);
    String &operator+=(const char *cstr);
    String &operator+=(char c);
    String &operator+=(unsigned char num);
    String &operator+=(int num);
    String &operator+=(unsigned int num);
    String &operator+=(long num);
    String &operator+=(unsigned long num);
    String &operator+=(float num);
    String &operator+=(double num);

    friend String &operator+(const String &lhs, const String &rhs);
    friend String &operator+(const String &lhs, const char *cstr);
    friend String &operator+(const String &lhs, char c);
    friend String &operator+(const String &lhs, unsigned char num);
    friend String &operator+(const String &lhs, int num);
    friend String &operator+(const String &lhs, unsigned int num);
    friend String &operator+(const String &lhs, long num);
    friend String &operator+(const String &lhs, unsigned long num);
    friend String &operator+(const String &lhs, float num);
    friend String &operator+(const String &lhs, double num);
    // String & operator + (const String &lhs, const __FlashStringHelper *rhs);
    // String & operator += (const __FlashStringHelper *str){ return (*this);}

    // const char *operator+(const char other[22]) { return ""; }
    // const char* operator+(const char other[]) {return "";}
    // char* operator+(const char* other) {return "";}
    // char* operator+(char* other) {return "";}
    // const char* operator+(char* other) {return "";}
    // String operator+(String other) {return String();}

    int length();
    void toCharArray(...);
    int indexOf(...);
    String substring(...);
    void trim();
    const char* c_str();
};

// #define NULL 0

// class WiFiClient
class WiFiClient
{
};

class HTTPClient
{
public:
    void begin(...);
    void end();
    int GET();
    String getString();
};


// #include <stdio.h> // for size_t
// class Print
// {
//   public:
//     size_t print(const __FlashStringHelper *);
//     size_t print(const String &);
//     size_t print(const char[]);
//     size_t print(char);
//     size_t print(unsigned char, int = DEC);
//     size_t print(int, int = DEC);
//     size_t print(unsigned int, int = DEC);
//     size_t print(long, int = DEC);
//     size_t print(unsigned long, int = DEC);
//     size_t print(double, int = 2);
//     size_t print(const Printable&);

//     size_t println(const __FlashStringHelper *);
//     size_t println(const String &s);
//     size_t println(const char[]);
//     size_t println(char);
//     size_t println(unsigned char, int = DEC);
//     size_t println(int, int = DEC);
//     size_t println(unsigned int, int = DEC);
//     size_t println(long, int = DEC);
//     size_t println(unsigned long, int = DEC);
//     size_t println(double, int = 2);
//     size_t println(const Printable&);
//     size_t println(void);

//     virtual void flush() { /* Empty implementation for backward compatibility */ }
// };
